package api

import common.CommonConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import
import kotlin.system.exitProcess

@Import(value = [CommonConfig::class])
@SpringBootApplication
class ApiMain(private val commonConfig: CommonConfig) {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<ApiMain>(*args) {
                if (System.getenv("SUSPEND") == "true") {
                    exitProcess(status = 0)
                }
            }
        }
    }

    fun getMessage(): String {
        return "${commonConfig.main()} called by api"
    }
}
