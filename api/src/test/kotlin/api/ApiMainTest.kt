package api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.Test
import kotlin.test.assertEquals

@SpringBootTest
class ApiMainTest @Autowired constructor(private val apiMain: ApiMain) {
    @Test
    fun getMessage() {
        assertEquals("common called by api", apiMain.getMessage())
    }
}
