package bat

import common.CommonConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import

@Import(value = [CommonConfig::class])
@SpringBootApplication
class BatMain(private val commonConfig: CommonConfig) {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println(BatMain(CommonConfig()).getMessage())
        }
    }

    fun getMessage(): String {
        return "${commonConfig.main()} called by bat"
    }
}
