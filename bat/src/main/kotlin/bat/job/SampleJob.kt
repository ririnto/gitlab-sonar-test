package bat.job

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.job.builder.JobBuilder
import org.springframework.batch.core.repository.JobRepository
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.builder.StepBuilder
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.PlatformTransactionManager

@Configuration(value = SampleJob.CONFIG)
@EnableBatchProcessing
class SampleJob {
    companion object {
        const val JOB: String = "SAMPLE"
        const val CONFIG: String = "$JOB-CONFIG"
        const val STEP: String = "$JOB-STEP"
        const val TASKLET: String = "$JOB-TASKLET"
    }

    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    @Bean(name = [JOB])
    fun job(
        jobRepository: JobRepository,
        @Qualifier(value = STEP) step: Step
    ): Job = JobBuilder(JOB, jobRepository)
        .start(step)
        .build()

    @Bean(name = [STEP])
    fun step(
        jobRepository: JobRepository,
        transactionManager: PlatformTransactionManager,
        @Qualifier(value = TASKLET) tasklet: Tasklet
    ): Step = StepBuilder(STEP, jobRepository)
        .tasklet(tasklet, transactionManager)
        .build()

    @Bean(name = [TASKLET])
    fun tasklet(): Tasklet =
        Tasklet { _: StepContribution, _: ChunkContext ->
            log.info("EMPTY TASKLET")

            RepeatStatus.FINISHED
        }
}