package bat

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.Test
import kotlin.test.assertEquals

@SpringBootTest
class BatMainTest @Autowired constructor(private val batMain: BatMain) {
    @Test
    fun getMessage() {
        assertEquals("common called by bat", batMain.getMessage())
    }
}
