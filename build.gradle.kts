import org.gradle.accessors.dm.LibrariesForLibs
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.plugin.ResolveMainClassName
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    alias(libs.plugins.git.properties) apply false

    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.jpa) apply false
    alias(libs.plugins.kotlin.spring) apply false

    alias(libs.plugins.spring.boot)
    alias(libs.plugins.spring.dependency.management)

    alias(libs.plugins.sonarqube)
    alias(libs.plugins.jacoco.report.aggregation)
}

group = "org.example"
version = "1.0-SNAPSHOT"

allprojects {
    repositories {
        mavenCentral()
    }
}

subprojects {
    val libs: LibrariesForLibs = rootProject.libs

    apply(plugin = libs.plugins.git.properties.get().pluginId)

    apply(plugin = libs.plugins.kotlin.jvm.get().pluginId)
    apply(plugin = libs.plugins.kotlin.jpa.get().pluginId)
    apply(plugin = libs.plugins.kotlin.spring.get().pluginId)

    apply(plugin = libs.plugins.spring.boot.get().pluginId)
    apply(plugin = libs.plugins.spring.dependency.management.get().pluginId)

    apply(plugin = libs.plugins.sonarqube.get().pluginId)
    apply(plugin = libs.plugins.jacoco.asProvider().get().pluginId)

    group = rootProject.group
    version = rootProject.version

    configurations {
        named<Configuration>(name = "compileOnly") {
            val annotationProcessor: Provider<Configuration> = named<Configuration>(name = "annotationProcessor")
            if (annotationProcessor.isPresent) {
                extendsFrom(annotationProcessor.get())
            }
        }
    }

    dependencies {
        implementation(platform(libs.spring.cloud.dependencies))
    }
}

allprojects {
    val libs: LibrariesForLibs = rootProject.libs

    extensions.findByType<JavaPluginExtension>()?.apply {
        sourceCompatibility = JavaVersion.VERSION_21
        targetCompatibility = JavaVersion.VERSION_21
    }

    extensions.findByType<JacocoPluginExtension>()?.apply {
        toolVersion = libs.versions.jacoco.get()
    }

    tasks {
        withType<Jar> {
            enabled = sourceSets.map(transform = SourceSet::getAllSource)
                .map(transform = SourceDirectorySet::getFiles)
                .flatten()
                .isNotEmpty()
        }

        withType<BootJar> {
            archiveBaseName.set(fullName)
            enabled = project.path in setOf(":api", ":bat")
        }

        withType<ResolveMainClassName> {
            enabled = withType<BootJar>().any(predicate = BootJar::isEnabled)
        }

        withType<JacocoReport> {
            reports {
                csv.required.set(false)
                html.required.set(true)
                xml.required.set(true)
            }
        }

        withType<KotlinCompile> {
            compilerOptions {
                javaParameters.set(true)
                freeCompilerArgs.add("-Xjsr305=strict")
                freeCompilerArgs.add("-Xjvm-default=all")
                jvmTarget.set(JvmTarget.JVM_21)
            }
        }

        withType<Test> {
            useJUnitPlatform()
        }
    }
}

dependencies {
    subprojects {
        jacocoAggregation(project(path = path))
    }
}

tasks {
    register("jacocoRootReport") {
        group = LifecycleBasePlugin.VERIFICATION_GROUP
        description = "test code coverage report for sonarqube"
        dependsOn(withType<JacocoReport>().map(transform = JacocoReport::getPath))
    }
}

val Project.fullName: String
    get() = parent?.let { project: Project ->
        "${project.fullName}-$name"
    } ?: name
