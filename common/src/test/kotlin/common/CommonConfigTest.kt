package common

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.assertEquals

@SpringBootTest(classes = [CommonConfig::class])
class CommonConfigTest @Autowired constructor(private val commonConfig: CommonConfig) {
    @Test
    fun test() {
        assertEquals("common", commonConfig.main())
    }
}
