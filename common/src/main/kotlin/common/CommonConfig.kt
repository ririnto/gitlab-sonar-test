package common

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CommonConfig {
    @Bean
    fun runner(): ApplicationRunner {
        return ApplicationRunner {
            println("Hello from common module")
        }
    }

    fun main(): String {
        return "common"
    }
}
