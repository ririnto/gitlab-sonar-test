dependencies {
    implementation(libs.bundles.jackson)
    implementation(libs.bundles.kotlin)
    implementation(libs.spring.boot.starter)
    annotationProcessor(libs.spring.boot.configuration.processor)
    testImplementation(libs.kotlin.test)
    testImplementation(libs.spring.boot.starter.test)
    testRuntimeOnly(libs.junit.platform.launcher)
}
